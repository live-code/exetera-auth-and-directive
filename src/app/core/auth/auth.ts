export interface Auth {
  displayName: string;
  role: 'admin' | 'guest' | 'public';
  token: string;
}

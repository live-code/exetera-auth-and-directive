import { Injectable } from '@angular/core';
import { User } from '../../model/user';
import { HttpClient } from '@angular/common/http';
import { Auth } from './auth';

@Injectable({ providedIn: 'root' })
export class AuthService {
  auth: Auth;

  constructor(private http: HttpClient) {}

  signInAs(signInAs: string): Promise<User> {
    return new Promise((resolve, reject) => {
      this.http.get<Auth>(`https://my-json-server.typicode.com/training-api/dynamic-menu/loginAs${signInAs}`)
        .subscribe(res => {
          this.auth = res;
          localStorage.setItem('auth', JSON.stringify(res));
          resolve(res);
        });
    });
  }

  get isLogged(): boolean {
    return !!localStorage.getItem('auth');
  }

  get loggedUser(): Auth {
    return JSON.parse(localStorage.getItem('auth'));
  }

  signOut() {
    this.auth = null;
    localStorage.removeItem('auth');
  }

  showByRole(role: string) {
    return this.isLogged && this.loggedUser?.role === role;
  }
}

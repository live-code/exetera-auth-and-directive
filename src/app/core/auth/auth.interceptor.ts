import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let clonedReq = req;
    if (this.authService.isLogged && this.authService.loggedUser) {
      clonedReq = req.clone({
        setHeaders: { Authorization: this.authService.loggedUser.token}
      });
    }
    return next.handle(clonedReq)
      .pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            console.log('err', err);
            switch (err.status) {
              case 401:
              case 404:
                this.router.navigateByUrl('login');
                break;
            }
          }
          return of(err)
        })
      )
  }
}


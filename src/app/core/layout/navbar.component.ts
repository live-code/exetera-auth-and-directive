import { ChangeDetectionStrategy, Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Menu } from '../../model/menu';
import { MenuAction } from '../../shared/components/super-menu.component';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-navbar',
  template: `
    <app-super-menu 
      *ngIf="menu; else spinner" 
      [data]="menu" 
      [userRole]="authService.loggedUser?.role" 
      (tabClick)="doSomething($event)"
    ></app-super-menu>
    
    <button *ngIf="authService.isLogged" (click)="signOutHandler()">logout</button>

    <ng-template #spinner>
      <div class="spinner-border" role="status" >
        <span class="sr-only">Loading...</span>
      </div>
    </ng-template>
    
  `,
  styles: [
  ]
})
export class NavbarComponent {
  menu: Menu;

  constructor(
    private http: HttpClient,
    private router: Router,
    public authService: AuthService
  ) {
    http.get<Menu>('https://my-json-server.typicode.com/training-api/dynamic-menu/menu')
      .subscribe(res => this.menu = res);
  }

  doSomething(event: MenuAction) {
    switch (event.action) {
      case 'router':
        this.router.navigateByUrl(event.params)
        break;
      case 'openURL':
        break;
    }
  }


  signOutHandler() {
    this.authService.signOut();
    this.router.navigateByUrl('login');
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { SharedModule } from './shared/shared.module';
import { NavbarComponent } from './core/layout/navbar.component';
import { AuthGuard } from './core/auth/auth.guard';
import { AuthInterceptor } from './core/auth/auth.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    SharedModule,
    RouterModule.forRoot([
      {
        path: 'login',
        loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)
      },
      { path: 'home', loadChildren: () => import('./features/home/home.module').then(m => m.HomeModule) },
      {
        path: 'admin',
        canActivate: [AuthGuard],
        loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule)
      },
      { path: 'admin-members/:id', loadChildren: () => import('./features/admin-members/admin-members.module').then(m => m.AdminMembersModule) },
      { path: 'contacts', loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule) },
      { path: '', redirectTo: 'login', pathMatch: 'full'},
      { path: '**', redirectTo: 'home'}
    ]),

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

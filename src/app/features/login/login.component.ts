import { Component, OnInit } from '@angular/core';
import { User } from '../../model/user';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../core/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  template: `
    <button (click)="signInAs('Admin')">ADmin</button>
    <button (click)="signInAs('Guest')">Guest</button>
    <button (click)="signOut()" *ngIf="user">Logout</button>
  `,
  styles: [
  ]
})
export class LoginComponent  {
  user: User;

  constructor(private authService: AuthService,  private router: Router) {
    // ....
  }

  signInAs(signInAs: string) {
    this.authService.signInAs(signInAs)
      .then(user => {
        this.router.navigateByUrl(user.role === 'admin' ? 'admin' : 'home')
      });
  }

  signOut() {
    this.user = null;
  }

}

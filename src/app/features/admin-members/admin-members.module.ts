import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminMembersComponent } from './admin-members.component';


const routes: Routes = [
  { path: '', component: AdminMembersComponent }
];

@NgModule({
  declarations: [AdminMembersComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class AdminMembersModule { }

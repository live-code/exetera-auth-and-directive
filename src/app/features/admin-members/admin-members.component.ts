import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-admin-members',
  template: `
    <pre>{{user | json}}</pre>
    
    <button routerLink="/admin-members/5">next</button>
  `,
  styles: [
  ]
})
export class AdminMembersComponent implements OnInit {
  user: any;

  constructor(http: HttpClient, activatedRoute: ActivatedRoute) {
    activatedRoute.params.subscribe(res => {
      http.get<any>('https://jsonplaceholder.typicode.com/users/' + res.id)
        .subscribe(u => this.user = u);
    });
  }

  ngOnInit(): void {
  }

}

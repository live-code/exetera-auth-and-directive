import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';

@Component({
  selector: 'app-home',
  template: `
    <input ngModel>
      <div
        [appXhr]="'https://jsonplaceholder.typicode.com/albums'"
        (xhrLoad)="data = $event"
        [appPadding]="0"
        [margin]="2"
      >home works!</div>
      
      <div [appColors]="colore" appBg="pink">{{data| json}}</div>
    
      <h1 *ngIf="authService.isLogged && this.authService.loggedUser?.role === 'admin'">
        sei un bellissimo admin!
      </h1>
      
      <h2 *ngIf="authService.showByRole('guest')">
        Sei un brutto e povero guest!
      </h2>
      
<!--
      <div [xhr]="url" (success)="doSomething($event)" />
-->
      
      <button [appUrl]="'http://www.google.com'">Visit web site</button>
      <!--<h3 *ifRoleIs="guest"></h3>-->
  `,
  styles: [
  ]
})
export class HomeComponent implements OnInit {
  data;
  colore = 'red'

  constructor(public authService: AuthService) {
    setTimeout(() => {
      this.colore = 'green'
    }, 2000)
  }

  ngOnInit(): void {
  }

}

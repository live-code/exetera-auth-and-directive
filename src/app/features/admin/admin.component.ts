import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-admin',
  template: `
    <li *ngFor="let user of users" [routerLink]="'/admin-members/' + user.id">
      {{user.name}}
    </li>
  `,
  styles: [
  ]
})
export class AdminComponent implements OnInit {
  users: any[];

  constructor(http: HttpClient) {
    http.get<any[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => this.users = res)
  }

  ngOnInit(): void {
  }

}

import { Component, ElementRef, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';


@Component({
  selector: 'app-root',
  template: `
    <app-navbar></app-navbar>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  constructor(private router: Router) {
    this.router.events.subscribe(res => {
      if(res instanceof NavigationEnd) {
        console.log('track', res)
      }
    })
  }
}

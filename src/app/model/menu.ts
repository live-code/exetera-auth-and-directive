export interface Menu {
  color: string;
  bg: string;
  items: Item[];
}
export interface Item {
  id: number;
  label: string;
  action: string;
  params: string;
  role: string;
  items?: Item[];
}

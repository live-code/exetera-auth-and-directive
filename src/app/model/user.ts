export interface User {
  displayName: string;
  role: 'admin' | 'guest' | 'public';
  token: string;
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperMenuComponent } from './components/super-menu.component';
import { PaddingDirective } from './directives/padding.directive';
import { UrlDirective } from './directives/url.directive';
import { XhrDirective } from './directives/xhr.directive';
import { ColorsDirective } from './directives/colors.directive';

@NgModule({
  declarations: [SuperMenuComponent, PaddingDirective, UrlDirective, XhrDirective, ColorsDirective],
  imports: [
    CommonModule
  ],
  exports: [SuperMenuComponent, PaddingDirective, UrlDirective, XhrDirective, ColorsDirective]
})
export class SharedModule { }

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Item, Menu } from '../../model/menu';

export interface MenuAction {
  action: string;
  params: string;
}

@Component({
  selector: 'app-super-menu',
  template: `
    <ul
      class="list-group list-group-flush"
    >
      <ng-container  *ngFor="let item of data?.items">
        <li 
          class="list-group-item"
          *ngIf="showByRole(item)"
          [style.color]="data.color"
          [style.backgroundColor]="data.bg"
          (click)="itemClickHandler($event, item)"
        >
          {{item.label}}
          <ul class="list-group list-group-flush" *ngIf="item.items">
            <ng-container *ngFor="let subitem of item.items">
              <li 
                class="list-group-item"
                *ngIf="showByRole(subitem)"
                [style.color]="data.color"
                [style.backgroundColor]="data.bg"
                (click)="itemClickHandler($event, subitem)"
              >
                {{subitem.label}}
              </li>
            </ng-container>
          </ul>
        </li>
      </ng-container>
    </ul>
  `,
})
export class SuperMenuComponent  {
  @Input() data: Menu;
  @Input() userRole: string;
  @Output() tabClick: EventEmitter<MenuAction> = new EventEmitter<any>()

  itemClickHandler(event: MouseEvent, item: Item) {
    event.stopPropagation();
    const { action, params } = item;
    this.tabClick.emit({ action, params });
  }

  showByRole(item: Item) {
    return item.role === 'public' ||
      this.userRole === 'admin' ||
      this.userRole === item.role;
  }
}

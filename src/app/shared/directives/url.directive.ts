import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appUrl]'
})
export class UrlDirective {
  @Input() appUrl: string;

  @HostListener('click')
  onClickHandler() {
    window.open(this.appUrl)
  }

  constructor() { }

}

import { Directive, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Directive({
  selector: '[appXhr]'
})
export class XhrDirective {
  @Output() xhrLoad: EventEmitter<any> = new EventEmitter<any>();
  @Input() set appXhr(val: string) {
    this.http.get(val)
      .subscribe(res => this.xhrLoad.emit(res));
  }
  constructor(private http: HttpClient) { }
}

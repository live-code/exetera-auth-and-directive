import { Directive, ElementRef, HostBinding, Input, OnChanges, Renderer2, SimpleChanges, ViewChild } from '@angular/core';

@Directive({
  selector: '[appColors]'
})
export class ColorsDirective implements OnChanges {
  @Input() appColors: string;
  @Input() appBg: string;
  /*@Input() set appColors(val: string) {
    this.renderer2.setStyle(this.elementRef.nativeElement, 'color', val);
  }
*/
  constructor(
    private elementRef: ElementRef,
    private renderer2: Renderer2
  ) {
    console.log(elementRef.nativeElement)
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.appColors) {
      this.renderer2.setStyle(this.elementRef.nativeElement, 'color', changes.appColors.currentValue);
    }

    if (changes.appBg) {
      this.renderer2.setStyle(this.elementRef.nativeElement, 'backgroundColor', changes.appBg.currentValue);
    }
  }
}

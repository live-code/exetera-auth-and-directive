import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appPadding]'
})
export class PaddingDirective {
  @Input() appPadding: number;
  @Input() margin: number;

  @HostBinding() className = 'alert alert-danger';
  @HostBinding('style.padding') get getPadding() {
    return this.appPadding * 10 + 'px';
  }

  @HostBinding('style.margin') get getMargin() {
    return this.margin * 10 + 'px';
  }
}
